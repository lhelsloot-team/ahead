#include "ahead.hpp"

#include "dsp.hpp"
#include "psp.hpp"
#include "timing.hpp"
#include "logging.hpp"
#include <core/secure_permutation.h>

namespace ahead
{
    std::unordered_map<unsigned, DSP*> DSP::campaign_map;

    Timer<timer_id::calculate_bid, false> bid_timer("calculate_bid");

    Campaign::Campaign(unsigned&& campaign_id_arg, BigInteger&& ad_arg, ThresholdPaillier::Ciphertext&& id_arg, BigInteger&& c1_arg, ThresholdPaillier::Ciphertext&& c2_arg)
        : w(UserProfile::profile_d, 0.1)
        , campaign_id(std::move(campaign_id_arg))
        , ad(std::move(ad_arg))
        , id(std::move(id_arg))
        , c1(std::move(c1_arg))
        , c2(std::move(c2_arg)) {}

	ThresholdPaillier::Ciphertext Campaign::bidding_function(ThresholdPaillier::Ciphertext yhat)
	{
		return (yhat * c1) + c2;
	}

	DSPBase::DSPBase(std::shared_ptr<PSP> psp_arg, std::shared_ptr<ThresholdPaillier> crypto)
        : ThresholdPaillierParty(crypto), psp(psp_arg) {}

	DSPBase::DSPBase(std::shared_ptr<PSP> psp_arg, std::shared_ptr<ThresholdPaillier> crypto, std::shared_ptr<Paillier> paillier_crypto)
        : ThresholdPaillierParty(crypto, paillier_crypto), psp(psp_arg) {}


    DSP::DSP(std::shared_ptr<PSP> psp_arg, std::shared_ptr<ThresholdPaillier> crypto, std::shared_ptr<Paillier> paillier_crypto)
        : DSPBase(psp_arg, crypto, paillier_crypto)
        , packer(crypto, fp::w) {}

	void DSP::bid(UserProfile const& user)
	{
		PartiallyDecryptedVector bids;
        bids.reserve(this->campaigns.size());
		for (size_t i = 0; i < this->campaigns.size(); i++)
		{
            auto bid = this->calculate_bid(this->campaigns[i], user);
            auto share = this->crypto_provider->ShareDecrypt(bid);
            bids.emplace_back(std::move(bid), std::move(share));
		}

		//Timer<timer_id::calculate_sigma> timer;
		SecurePermutation permutation(bids.size());
		permutation.Permute(bids);

		EncryptedVector yhat = this->psp->calculate_sigma(bids);

		permutation.InvertPermutation(yhat);
        //timer.stop();

		for (size_t i = 0; i < this->campaigns.size(); i++)
		{
			auto bid = this->campaigns[i].bidding_function(yhat[i]);

			BidResponse response;
            response.has_ad = true;
			response.ad = user.crypto_provider->EncryptInteger(this->campaigns[i].get_ad());
			response.bid_price = this->crypto_provider->RandomizeCiphertext(bid);
			response.yhat = this->crypto_provider->RandomizeCiphertext(yhat[i]);
			response.campaign_id = this->psp->get_paillier_crypto()->RandomizeCiphertext(this->campaigns[i].get_id());

			this->psp->submit_bid(response);
		}
	}

	void DSP::bid(unsigned user_id)
    {
        this->bid(this->user_profiles.at(user_id));
#ifdef NO_USER_PROFILE_CACHE
        this->user_profiles.clear();
#endif
    }

	void DSP::add_campaign(BigInteger&& ad, unsigned&& id, BigInteger&& c1, BigInteger&& c2)
    {
        this->campaigns.emplace_back(
            std::move(id),
            std::move(ad),
            this->psp->get_paillier_crypto()->EncryptInteger(id),
            std::move(c1),
            this->crypto_provider->EncryptInteger(c2)
        );
        campaign_map[id] = this;
        this->psp->clear_model_aggregate(id);
    }

	ThresholdPaillier::Ciphertext DSP::calculate_bid(Campaign const& campaign, UserProfile const& user)
	{
        unsigned w_count = 0;
        bid_timer.start();
		ThresholdPaillier::Ciphertext wTx = this->crypto_provider->GetEncryptedZero(false);
		for (size_t i = 0; i < UserProfile::profile_d; i++)
		{
            // Skip if w[i] == 0
            if (std::abs(campaign.w[i]) >= fp::min_value)
            {
                w_count++;
                wTx = wTx + (user.profile.at(i) * fp::to_signed_biginteger(campaign.w[i]));
            }
		}
        bid_timer.stop();
        //Logger::GetInstance().push("w_count", w_count);

		return wTx;
	}

	void DSP::update_model(DataPacker::PackedData const& g, unsigned k)
    {
        DataPacker::UnpackedData plain_g = this->packer.unpack(g);

        for (auto it = this->campaigns.begin(); it != this->campaigns.end(); it++)
        {
            if (it->campaign_id == k)
            {
                for (unsigned i = 0; i < UserProfile::profile_d; i++)
                {
                    if (plain_g[i] != 0)
                    {
                        it->w[i] = it->w[i] - fp::to_double(plain_g[i]) / 50; //(mpz_get_si(plain_g[i].data) >> 4);
                        //std::cout << "w[" << i << "] = " << it->w[i] << std::endl;
                    }
                }
                break;
            }
        }
    }

    void DSP::add_user(unsigned user_id, std::shared_ptr<Paillier> crypto)
    {
        this->user_profiles.emplace(user_id, crypto);
    }

    void DSP::update_user_profile(EncryptedArray const& profile, Paillier::Ciphertext const& user_id_enc, Paillier::Ciphertext const& r_enc)
    {
        unsigned long r = this->paillier_crypto_provider->DecryptInteger(r_enc).ToUnsignedLong();
        unsigned long user_id = this->paillier_crypto_provider->DecryptInteger(user_id_enc).ToUnsignedLong();

        UserProfile& old_profile = this->user_profiles.at(user_id);

        for (unsigned i = 0; i < UserProfile::profile_d; i++)
        {
#ifdef INCREMENTAL_PROFILE_UPDATES
            old_profile.profile.at(i) = profile.at((i + r) % UserProfile::profile_d) + old_profile.profile.at(i);
#else
            old_profile.profile.at(i) = profile.at((i + r) % UserProfile::profile_d);
#endif
        }
    }

    void DSP::update_user_profile(EncryptedArray&& profile, Paillier::Ciphertext const& user_id_enc, Paillier::Ciphertext const& r_enc)
    {
        unsigned long r = this->paillier_crypto_provider->DecryptInteger(r_enc).ToUnsignedLong();
        unsigned long user_id = this->paillier_crypto_provider->DecryptInteger(user_id_enc).ToUnsignedLong();

        UserProfile& old_profile = this->user_profiles.at(user_id);

#ifdef INCREMENTAL_PROFILE_UPDATES
        for (unsigned i = 0; i < UserProfile::profile_d; i++)
        {
            old_profile.profile.at(i) = profile.at((i + r) % UserProfile::profile_d) + old_profile.profile.at(i);
        }
#else
        std::rotate(profile.begin(), profile.begin() + static_cast<long>(r), profile.end());
        old_profile.profile = std::move(profile);
#endif
    }

    DSP* DSP::find_dsp(unsigned id)
    {
        return campaign_map.at(id);
    }

}

