#ifndef AHEAD_TIMING_HPP
#define AHEAD_TIMING_HPP

#include <chrono>

namespace ahead
{
    void timing_main();

    enum class timer_id
    {
        update_profile = 0,
        request_ad,
        model_update,
        calculate_bid,
        expand_profile,
        get_model_update,

        number_of_timers
    };

    class BaseTimer
    {
    public:
        static void print_timers();

    protected:
        BaseTimer(timer_id id, std::string&& name_arg);
        void update(timer_id id, std::chrono::nanoseconds duration, unsigned reps);

        static std::array<std::string, static_cast<unsigned>(timer_id::number_of_timers)> names;
        static std::array<std::chrono::nanoseconds, static_cast<unsigned>(timer_id::number_of_timers)> elapsed;
        static std::array<unsigned, static_cast<unsigned>(timer_id::number_of_timers)> count;
    };

    template<timer_id ID, bool enabled>
    class Timer : BaseTimer
    {
    public:
        Timer(std::string&& name) : BaseTimer(ID, std::move(name)) {}

        void run(std::function<void()> f, unsigned reps=1)
        {
            if (enabled)
            {
                this->start();
                for (unsigned i = 0; i < reps; i++)
                {
                    f();
                }
                this->stop(reps);
            }
            else
            {
                f();
            }
        }

        void start()
        {
            if (enabled)
            {
                start_time = std::chrono::high_resolution_clock::now();
            }
        }

        void stop(unsigned reps=1)
        {
            if (enabled)
            {
                auto end_time = std::chrono::high_resolution_clock::now();
                this->update(ID, std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - this->start_time), reps);
            }
        }

        void skip()
        {
            if (enabled)
            {
                this->update(ID, std::chrono::nanoseconds(0), 1);
            }
        }

    private:
        std::chrono::high_resolution_clock::time_point start_time;
    };
}

#endif
