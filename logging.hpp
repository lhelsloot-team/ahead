#ifndef AHEAD_LOGGING_HPP
#define AHEAD_LOGGING_HPP

#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>

namespace ahead
{

class Logger
{
public:
    static Logger& GetInstance();

    Logger();

    void flush();

    template<typename T>
    void push(std::string const& name, T&& data)
    {
        if (!this->wrote_head)
        {
            this->keys.push_back(name);
        }

        if (!this->first)
        {
            this->buffer << ",";
        }
        else
        {
            this->first = false;
        }
        this->buffer << data;
    }

private:
    static Logger instance;

    bool first, wrote_head;
    std::vector<std::string> keys;
    std::stringstream buffer;
    std::ofstream log_stream;
};

}

#endif
