#include "data_packer.hpp"
#include <cmath>

namespace ahead
{

DataPacker::PackedData& DataPacker::PackedData::operator+= (DataPacker::PackedData const& other)
{
    auto lit = this->data.begin();
    for (auto rit = other.data.begin(); lit != this->data.end() && rit != other.data.end(); lit++, rit++)
    {
        *lit = *lit + *rit;
    }
    return *this;
}

DataPacker::DataPacker(std::shared_ptr<ThresholdPaillier> crypto, size_t size)
    : crypto_provider(crypto)
    , data_size(size)
    , elements_per_bucket((crypto->GetMessageSpaceSize() - 1) / (data_size + 1)) {}

void DataPacker::push_back(ThresholdPaillier::Ciphertext const& element)
{
    ThresholdPaillier::Ciphertext el;

    if (this->buffer_size)
    {
        BigInteger base(2);
        el = element * base.Pow(this->buffer_size * this->get_element_size());
    }
    else
    {
        el = element;
    }

    if (this->buffer_contains_encryption)
    {
        this->ciphertext_buffer = this->ciphertext_buffer + el;
    }
    else
    {
        this->ciphertext_buffer = el;
        this->buffer_contains_encryption = true;
    }

    this->buffer_size++;
    this->check_size();
}

void DataPacker::push_back(BigInteger const& element)
{
    if (element != 0)
    {
        this->plaintext_buffer += (element << (this->buffer_size * this->get_element_size()));
    }
    this->buffer_size++;
    this->check_size();
}

DataPacker::PackedData DataPacker::extract()
{
    size_t size = this->buffer_size;
    if (size > 0)
    {
        this->encrypt_bucket();
        //this->packed_data.data.emplace_back(this->crypto_provider->EncryptInteger(size));
    }
    /*else
    {
        if (this->packed_data.data.size() > 0)
        {
            this->packed_data.data.emplace_back(this->crypto_provider->EncryptInteger(this->elements_per_bucket));
        }
    }*/

    PackedData return_value;
    this->packed_data.data.swap(return_value.data);
    return return_value;
}

DataPacker::UnpackedData DataPacker::unpack(DataPacker::PackedData const& packed)
{
    UnpackedData unpacked;

    if (!packed.data.empty())
    {
        //size_t last_bucket_size = this->crypto_provider->DecryptInteger(packed_data.data.back()).ToUnsignedLong();

        for (auto it = packed.data.begin(); it != packed.data.end(); it++)
        {
            this->unpack_bucket(*it, unpacked);
        }
        //this->unpack_bucket (*(it++), last_bucket_size, unpacked);
    }

    return unpacked;
}

void DataPacker::check_size()
{
    if (this->buffer_size == this->elements_per_bucket)
    {
        this->encrypt_bucket();
    }
}

void DataPacker::encrypt_bucket()
{
    ThresholdPaillier::Ciphertext bucket;

    if (this->crypto_provider->GetMessageSpaceUpperBound() < this->plaintext_buffer)
    {
        throw std::runtime_error("Data bucket does not fit into message space");
    }

    if (this->plaintext_buffer != 0)
    {
        bucket = this->crypto_provider->EncryptIntegerNonrandom(this->plaintext_buffer);
    }
    else
    {
        bucket = this->crypto_provider->GetEncryptedZero(false);
    }

    if (this->buffer_contains_encryption)
    {
        bucket = bucket + this->ciphertext_buffer;
    }

    bucket = this->crypto_provider->RandomizeCiphertext(bucket);
    this->packed_data.data.push_back(std::move(bucket));

    this->buffer_size = 0;
    this->plaintext_buffer = 0;
    this->buffer_contains_encryption = false;
}

void DataPacker::unpack_bucket(ThresholdPaillier::Ciphertext const& bucket, UnpackedData& unpacked)
{
    BigInteger plaintext = this->crypto_provider->DecryptBlob(bucket);
    unsigned mask = static_cast<unsigned>((1 << this->data_size) - 1);
    for (unsigned i = 0; i < this->elements_per_bucket; i++)
    {
        unpacked.emplace_back(plaintext & mask);
        plaintext >>= this->get_element_size();
    }
}



}
