#ifndef AHEAD_DSP_HPP
#define AHEAD_DSP_HPP

#include "ahead.hpp"
#include "threshold_paillier.hpp"
#include "psp.hpp"
#include "user.hpp"
#include "data_packer.hpp"

namespace ahead
{

class Campaign
{
public:
    Campaign(unsigned&& campaign_id, BigInteger&& ad, ThresholdPaillier::Ciphertext&& id, BigInteger&& c1, ThresholdPaillier::Ciphertext&& c2);

	ThresholdPaillier::Ciphertext bidding_function(ThresholdPaillier::Ciphertext yhat);
    void update(EncryptedArray const& g);

	BigInteger const& get_ad() const { return this->ad; }
	ThresholdPaillier::Ciphertext const& get_id() const { return this->id; }

	std::vector<float> w;
    unsigned campaign_id;

private:
	BigInteger ad;
	ThresholdPaillier::Ciphertext id;

	BigInteger c1;
	ThresholdPaillier::Ciphertext c2;
};

class DSPBase : public ThresholdPaillierParty
{
public:
    DSPBase(std::shared_ptr<PSP> psp_arg, std::shared_ptr<ThresholdPaillier> crypto);
    DSPBase(std::shared_ptr<PSP> psp_arg, std::shared_ptr<ThresholdPaillier> crypto, std::shared_ptr<Paillier> paillier_crypto);

protected:
    std::shared_ptr<PSP> psp;
};


class DSP : public DSPBase
{
public:
    DSP(std::shared_ptr<PSP> psp_arg, std::shared_ptr<ThresholdPaillier> crypto, std::shared_ptr<Paillier> paillier_crypto);

    void add_campaign(BigInteger&& ad, unsigned&& id, BigInteger&& c1, BigInteger&& c2);

	void bid(UserProfile const& user);
    void bid(unsigned user_id);

    void update_model(DataPacker::PackedData const& g, unsigned id);

    void add_user(unsigned user_id, std::shared_ptr<Paillier> crypto);

    void update_user_profile(EncryptedArray const& profile, Paillier::Ciphertext const& user_id, Paillier::Ciphertext const& r);
    void update_user_profile(EncryptedArray&& profile, Paillier::Ciphertext const& user_id, Paillier::Ciphertext const& r);

    static DSP* find_dsp(unsigned campaign_id);

private:
	ThresholdPaillier::Ciphertext calculate_bid(Campaign const& campaign, UserProfile const& user);

	std::vector<Campaign> campaigns;
    std::unordered_map<unsigned, UserProfile> user_profiles;
    static std::unordered_map<unsigned, DSP* > campaign_map;
    DataPacker packer;
};

}

#endif
