
#include "threshold_paillier.hpp"
#include <utils/config.h>

using namespace SeComLib::Utils;

namespace ahead
{
	ThresholdPaillier::ThresholdPaillier()
		: Paillier(), has_key_pair(false), has_key_share (false) {}

	ThresholdPaillier::ThresholdPaillier(PaillierPublicKey const& public_key)
		: Paillier(public_key), has_key_pair(false), has_key_share (false) {}

	ThresholdPaillier::ThresholdPaillier(PaillierPublicKey const& public_key, PrivateKeyShare const& key_share_arg)
		: Paillier(public_key), has_key_pair(false), has_key_share (true), key_share (key_share_arg) {}

	bool ThresholdPaillier::GenerateKeys()
	{
        BigInteger p, pprime, q, qprime, n;

        std::string p_str = Config::GetInstance().GetParameter("ThresholdPaillier.pprime", std::string(""));
        std::string q_str = Config::GetInstance().GetParameter("ThresholdPaillier.qprime", std::string(""));

        if (p_str.empty() || q_str.empty())
        {
            std::cout << "No keys found in config file, generating fresh ones" << std::endl;

            /// Set the length of primes p and q
            unsigned int primeLength = this->keyLength / 2;

            // Pick primes p and q such that p = 2p'+1 and q = 2q'+1, where p' and
            // q' are also prime.
            do
            {
                do
                {
                    p = RandomProvider::GetInstance().GetMaxLengthRandomPrime(primeLength);
                    pprime = p - 1;
                    pprime /= 2;
                } while (!pprime.IsPrime());

                do
                {
                    q = RandomProvider::GetInstance().GetMaxLengthRandomPrime(primeLength);
                    qprime = q - 1;
                    qprime /= 2;
                }
                while (!qprime.IsPrime());

                n = p * q;
            } while (n.GetSize() != this->keyLength || p == q || pprime == q || p == qprime);

            std::cout << "pprime: " << pprime.ToString(16) << std::endl;
            std::cout << "qprime: " << qprime.ToString(16) << std::endl;
        }
        else
        {
            std::cout << "Using keys from config file" << std::endl;

            pprime = BigInteger(p_str, 16);
            qprime = BigInteger(q_str, 16);
            p = pprime * 2 + 1;
            q = qprime * 2 + 1;
            n = p * q;
        }

		this->privateKey.p = p;
		this->privateKey.q = q;
		this->publicKey.n = n;

		this->nMinusOne = this->publicKey.n - 1;
		this->nSquared = this->publicKey.n.GetPow(2);
		this->nSquaredMinusOne = this->nSquared - 1;

		this->m = pprime * qprime;

		this->publicKey.g = this->publicKey.n + 1;
		this->has_key_pair = true;

		this->doPrecomputations();

		return true;
	}

	ThresholdPaillier::PrivateKeyShare ThresholdPaillier::GenerateKeyShares()
	{
		BigInteger l, linv, d, d0, d1;

		if (!this->has_key_pair)
		{
			this->GenerateKeys();
		}

		// λ seems to be 2m
		l = 2*this->m;

		// d should be 0 mod m and 1 mod n.
		// To be safe, set d to be 0 mod λ as well, so d = (λ^-1 mod n)(λ)
		linv = l.GetInverseModN(this->publicKey.n);
		d = linv*l;

		// Generate random additive shares of d
        d0 = RandomProvider::GetInstance().GetRandomInteger(d.GetSize() + Config::GetInstance().GetParameter<size_t>("ThresholdPaillier.kappa", 40));
		d1 = d - d0;

		this->key_share = d0;
		this->has_key_share = true;

		return d1;
	}


	ThresholdPaillier::DecryptedShare ThresholdPaillier::ShareDecrypt(ThresholdPaillier::Ciphertext const& ciphertext) const
	{
		if (!this->has_key_share ) {
			throw std::runtime_error("This operation requires a key share.");
		}

		DecryptedShare share(this->encryptionModulus);
		share.data = ciphertext.data.GetPowModN(this->key_share, this->nSquared);
		return share;
	}


	BigInteger ThresholdPaillier::CombineShares(ThresholdPaillier::DecryptedShare const& share1, ThresholdPaillier::DecryptedShare const& share2) const
	{
		BigInteger output;

		output = this->L((share1.data * share2.data) % this->nSquared, this->publicKey.n);

        if (output > this->positiveNegativeBoundary) {
            output -= this->GetMessageSpaceUpperBound();
        }

		return output;
	}

	BigInteger ThresholdPaillier::CombineToBlob(ThresholdPaillier::DecryptedShare const& share1, ThresholdPaillier::DecryptedShare const& share2) const
	{
        BigInteger output;

        output = this->L((share1.data * share2.data) % this->nSquared, this->publicKey.n);

        return output;
    }

	BigInteger ThresholdPaillier::DecryptInteger(Ciphertext const& ciphertext) const
    {
        BigInteger output = this->DecryptBlob(ciphertext);

        if (output > this->positiveNegativeBoundary) {
            output -= this->GetMessageSpaceUpperBound();
        }

        return output;
    }

    BigInteger ThresholdPaillier::DecryptBlob(Ciphertext const& ciphertext) const
    {
        if (!this->other_party)
        {
            throw std::runtime_error("This operation requires another party");
        }

        DecryptedShare my_share = this->ShareDecrypt(ciphertext);
        DecryptedShare their_share = this->other_party->get_share(ciphertext);
        return this->CombineToBlob(my_share, their_share);
    }

    void ThresholdPaillier::SetOtherParty(std::shared_ptr<ThresholdPaillierParty> party)
    {
        this->other_party = party;
    }


    ThresholdPaillierParty::ThresholdPaillierParty()
        : crypto_provider(std::make_shared<ThresholdPaillier>())
        , paillier_crypto_provider(std::make_shared<Paillier>())
    {
        this->paillier_crypto_provider->GenerateKeys();
        paillier_public_crypto_provider = std::make_shared<Paillier>(paillier_crypto_provider->GetPublicKey());
    }

    ThresholdPaillierParty::ThresholdPaillierParty(std::shared_ptr<ThresholdPaillier> crypto)
        : crypto_provider(crypto)
        , paillier_crypto_provider(std::make_shared<Paillier>())
    {
        this->paillier_crypto_provider->GenerateKeys();
        paillier_public_crypto_provider = std::make_shared<Paillier>(paillier_crypto_provider->GetPublicKey());
    }

    ThresholdPaillierParty::ThresholdPaillierParty(std::shared_ptr<ThresholdPaillier> crypto, std::shared_ptr<Paillier> paillier_crypto)
        : crypto_provider(crypto)
        , paillier_crypto_provider(paillier_crypto)
    {
        paillier_public_crypto_provider = std::make_shared<Paillier>(paillier_crypto_provider->GetPublicKey());
    }

    void ThresholdPaillierParty::set_other_party(std::shared_ptr<ThresholdPaillierParty> party)
    {
        this->crypto_provider->SetOtherParty(party);
    }

    PaillierPublicKey const& ThresholdPaillierParty::get_public_key() const
    {
        return this->crypto_provider->GetPublicKey();
    }

    PaillierPublicKey const& ThresholdPaillierParty::get_paillier_public_key() const
    {
        return this->paillier_crypto_provider->GetPublicKey();
    }

    std::shared_ptr<Paillier> ThresholdPaillierParty::get_paillier_crypto() const
    {
        return this->paillier_public_crypto_provider;
    }

    ThresholdPaillier::DecryptedShare ThresholdPaillierParty::get_share(Paillier::Ciphertext const& ciphertext) const
    {
        return this->crypto_provider->ShareDecrypt(ciphertext);
    }

    void ThresholdPaillierParty::set_crypto_provider(std::shared_ptr<ThresholdPaillier> crypto)
    {
        this->crypto_provider = crypto;
    }

}
