#include "fixed_point.hpp"
#include <utils/config.h>

using namespace SeComLib::Core;
using namespace SeComLib::Utils;

const size_t fp::w = Config::GetInstance().GetParameter<size_t>("HomomorphicAds.weight_bits", 16);
const size_t fp::p = Config::GetInstance().GetParameter<size_t>("HomomorphicAds.weight_integer_bits", 4);
const size_t fp::mult_factor = static_cast<size_t>(1 << (fp::w - fp::p - 1));
const long fp::fp_space = 1 << fp::w;
const long fp::signed_boundary = 1 << (fp::w - 1);
const double fp::min_value = 1. / fp::mult_factor;

unsigned fp::to_fp (double n)
{
    int r = static_cast<int> (std::round (n * mult_factor));

    if (r < 0)
    {
        r += fp_space;
    }

    else if (r >= fp_space)
    {
        throw std::runtime_error ("to_fp argument out of bounds");
    }

    return static_cast<unsigned> (r);
}

double fp::to_double (unsigned long n)
{
    n &= static_cast<unsigned>(fp_space - 1);

    signed r = static_cast<signed> (n);

    if (r >= signed_boundary)
    {
        r -= fp_space;
    }

    return static_cast<double> (r) / mult_factor;
}

double fp::to_double (BigInteger const& n)
{
    BigInteger i = n & static_cast<long>(fp_space - 1);
    return to_double(i.ToUnsignedLong());
}

BigInteger fp::to_signed_biginteger (double n)
{
    return BigInteger (static_cast<long> (std::round (n * mult_factor)));
}

