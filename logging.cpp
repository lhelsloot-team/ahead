#include "logging.hpp"
#include <iomanip>
#include <sstream>
#include <utils/config.h>

namespace ahead
{

using namespace SeComLib::Utils;

Logger Logger::instance;

Logger& Logger::GetInstance()
{
    return instance;
}

Logger::Logger()
    : first(true)
    , wrote_head(false)
{
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::stringstream name;
    name << "log_"
        << Config::GetInstance().GetParameter<unsigned>("HomomorphicAds.profile_bits")
        << "bits_"
        << Config::GetInstance().GetParameter<unsigned>("HomomorphicAdsTiming.dsp_count")
        << "dsps_"
        << Config::GetInstance().GetParameter<unsigned>("HomomorphicAdsTiming.campaigns_per_dsp")
        << "campaigns_"
        << std::put_time(&tm, "%Y%m%d_%H%M%S")
        << ".log";

    this->log_stream.open(name.str());
    this->log_stream.exceptions(std::ofstream::failbit | std::ofstream::badbit);
}

void Logger::flush()
{
    if (!this->wrote_head)
    {
        std::copy(this->keys.begin(), this->keys.end() - 1, std::ostream_iterator<std::string>(this->log_stream, ","));
        this->log_stream << this->keys.back() << std::endl;
        this->wrote_head = true;
    }

    this->buffer << std::endl << std::flush;
    this->log_stream << this->buffer.rdbuf() << std::flush;
    std::stringstream().swap(this->buffer);
    this->buffer.exceptions(std::stringstream::failbit | std::stringstream::badbit);
    this->first = true;
}

}
