#ifndef AHEAD_BID_HPP
#define AHEAD_BID_HPP

#include "ahead.hpp"
#include "threshold_paillier.hpp"

namespace ahead
{

struct BidResponse
{
    bool has_ad;
    Paillier::Ciphertext ad;
    ThresholdPaillier::Ciphertext bid_price;
    ThresholdPaillier::Ciphertext yhat;
    ThresholdPaillier::Ciphertext campaign_id;
};

}

#endif
