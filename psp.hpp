#ifndef AHEAD_PSP_HPP
#define AHEAD_PSP_HPP

#include <vector>
#include <unordered_map>

#include <private_recommendations_utils/secure_comparison_client.h>

#include "bid.hpp"
#include "threshold_paillier.hpp"
#include "data_packer.hpp"
#include "user.hpp"


namespace ahead
{

using namespace SeComLib::PrivateRecommendationsUtils;

struct ModelUpdate
{
    DataPacker::PackedData model;
    Paillier::Ciphertext price;
    unsigned count;
};

class DSP;

class PSP : public ThresholdPaillierParty
{
public:
    using ThresholdPaillierParty::ThresholdPaillierParty;

    ThresholdPaillier::PrivateKeyShare generate_key_shares();
    DgkPublicKey const& get_dgk_public_key() const;

    void expand_profile(std::shared_ptr<DSP> dsp, EncryptedCompressedProfile const& profile, Paillier::Ciphertext const& user_id);
    void expand_profile(std::vector<std::shared_ptr<DSP>> const& dsps, EncryptedCompressedProfile const& profile, Paillier::Ciphertext const& user_id);
    void no_expand_profile() const;

	EncryptedVector calculate_sigma(PartiallyDecryptedVector const& enc_s);
	void submit_bid(BidResponse const& bid_response);

	void start_bidding();
	std::vector<BidResponse>& collect_bids();

    void clear_model_aggregate(unsigned campaign_id);
    void aggregate_update(DataPacker::PackedData&& g, Paillier::Ciphertext const& campaign_id, ThresholdPaillier::Ciphertext const& bid_price);

    //std::shared_ptr<ThresholdPaillier> get_crypto_provider() { return this->crypto_provider; }

    std::shared_ptr<SecureComparisonClient> comparison_client;

private:
    EncryptedArray do_expand_profile(EncryptedCompressedProfile const& profile);

    Dgk dgk_crypto_provider;
	std::vector<BidResponse> bids;
    std::unordered_map<unsigned, ModelUpdate> model_aggregate;
    static const unsigned model_update_interval;
};

}

#endif
