#ifndef AHEAD_AD_EXCHANGE_HPP
#define AHEAD_AD_EXCHANGE_HPP

#include "ahead.hpp"
#include "user.hpp"
#include "dsp.hpp"
#include <core/paillier.h>
#include <private_recommendations_utils/secure_comparison_server.h>

namespace ahead
{

using namespace SeComLib::PrivateRecommendationsUtils;

class AdExchange : public DSPBase
{
public:
    AdExchange(std::shared_ptr<PSP> psp_arg, std::shared_ptr<ThresholdPaillier> crypto);

    std::shared_ptr<DSP> create_dsp();

    BidResponse request_ad(unsigned user_id);
    BidResponse request_ad(UserProfile const& user);

private:
    BidResponse perform_auction(std::vector<BidResponse>& bids);

    std::vector<std::shared_ptr<DSP> > dsps;
    Dgk dgk_crypto_provider;
    std::shared_ptr<SecureComparisonServer> comparison_server;
};

}

#endif
