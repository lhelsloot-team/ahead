
#include <utils/config.h>
#include <utils/cpu_timer.h>

#include "user.hpp"
#include "MurmurHash3.h"
#include "dsp.hpp"
#include "logging.hpp"

//#define NO_PROFILE_UPDATE

namespace ahead
{

using namespace SeComLib::Utils;

const size_t UserProfile::profile_bits = Config::GetInstance().GetParameter<size_t>("HomomorphicAds.profile_bits", 20);
const size_t UserProfile::profile_d = static_cast<size_t>(1 << UserProfile::profile_bits);
const unsigned UserImpersonator::profile_update_interval = Config::GetInstance().GetParameter<unsigned>("HomomorphicAds.profile_update_interval", 5);

UserProfile::UserProfile(std::shared_ptr<Paillier> crypto)
    : crypto_provider(crypto)
    , profile(profile_d) {}


void User::update_profile(CompressedProfile const& update)
{
    for (auto it = update.begin(); it != update.end(); it++)
    {
        int& v = this->profile[it->first];
        if (it->second != v)
        {
#ifdef USE_INCREMENTAL_UPDATES
            int i_diff = it->second - v;
            this->diff[it->first] += i_diff;
#endif
            v = it->second;
        }
    }
    this->count++;
}

CompressedProfile User::get_profile_diff()
{
#ifdef USE_INCREMENTAL_UPDATES
    CompressedProfile ret;
    ret.swap(this->diff);
    return ret;
#else
    return this->profile;
#endif
}

UserImpersonator::UserImpersonator(std::shared_ptr<ThresholdPaillier> threshold_crypto)
    : seed(42)
    , crypto_provider(std::make_shared<Paillier>())
    , threshold_crypto_provider(threshold_crypto)
    , encrypted_minus_one(threshold_crypto->EncryptIntegerNonrandom(fp::to_fp(-1)))
    , packer(threshold_crypto, fp::w)
{
    this->crypto_provider->GenerateKeys();
}

void UserImpersonator::update_profile(std::shared_ptr<PSP> psp, std::vector<std::shared_ptr<DSP>> const& dsps, DataLine const& data_line)
{
    unsigned user_id = data_line.user_id;

#ifdef NO_USER_PROFILE_CACHE
    this->users.clear();
#endif

    User& user = this->users[user_id];

    user.update_profile(this->get_hashed_profile(data_line));

#ifndef NO_USER_PROFILE_CACHE
    unsigned count = user.get_count();
    if (count % profile_update_interval == 1)
    {
        // Register new user with DSPs
        if (count == 1)
#endif
        {
            for (auto it = dsps.begin(); it != dsps.end(); it++)
            {
                std::shared_ptr<DSP> dsp = *it;
                dsp->add_user(user_id, this->crypto_provider);
            }
        }

        std::shared_ptr<Paillier> dsp_crypto = dsps[0]->get_paillier_crypto();
        EncryptedCompressedProfile ep = this->encrypt_hashed_profile(user.get_profile_diff(), dsp_crypto);
        //EncryptedCompressedProfile ep = this->encrypt_hashed_profile(user.get_profile());
        psp->expand_profile(dsps, ep, dsp_crypto->EncryptInteger(user_id));
#ifndef NO_USER_PROFILE_CACHE
    }
    else
    {
        psp->no_expand_profile();
    }
#endif
}

CompressedProfile const& UserImpersonator::get_profile(unsigned user_id) const
{
    return this->users.at(user_id).get_profile();
}

CompressedProfile UserImpersonator::get_hashed_profile(DataLine const& data_line)
{
    CompressedProfile profile;

    for (auto it = data_line.profile_data.begin(); it != data_line.profile_data.end(); it++)
    {
        uint32_t out;
        std::string value = it->first + it->second;

        MurmurHash3_x86_32(value.c_str(), value.length(), this->seed, &out);

        //uint32_t sign = out >> 31;

        out %= UserProfile::profile_d;
        //if (sign)
        //{
        //    profile[out] = profile[out] - 1;
        //}
        //else
        //{
            profile[out] = profile[out] + 1;
        //}
    }

    return profile;
}

EncryptedCompressedProfile UserImpersonator::encrypt_hashed_profile(CompressedProfile const& profile, std::shared_ptr<Paillier> dsp_crypto)
{
    EncryptedCompressedProfile encrypted_profile;
    BigInteger r_bigint = RandomProvider::GetInstance().GetRandomInteger(UserProfile::profile_bits);
    unsigned r = r_bigint.ToUnsignedLong();
    encrypted_profile.r = dsp_crypto->EncryptInteger(r_bigint);

    //std::cout << "User profile: " << std::endl;
    for (auto it = profile.begin(); it != profile.end(); it++)
    {
        if (it->second == 1)
        {
            encrypted_profile.data[(it->first + r) % UserProfile::profile_d] = this->threshold_crypto_provider->GetEncryptedOne(true);
        }
        else if (it->second != 0)
        {
            encrypted_profile.data[(it->first + r) % UserProfile::profile_d] = this->threshold_crypto_provider->EncryptInteger(it->second);
        }

    //    std::cout << it->first << " = " << it->second << std::endl;
    }
    //std::cout << std::endl;

    return encrypted_profile;
}

UserProfile UserImpersonator::get_encrypted_profile(CompressedProfile const& compressed_profile)
{
    UserProfile profile(this->crypto_provider);

    for (unsigned i = 0; i < UserProfile::profile_d; i++)
    {
        if (compressed_profile.count(i))
        {
            int d = compressed_profile.at(i);

            if (d == 0)
            {
                profile.profile.at(i) = this->threshold_crypto_provider->GetEncryptedZero(true);
            }
            else// if (d == 1)
            {
                profile.profile.at(i) = this->threshold_crypto_provider->GetEncryptedOne(true);
            }
            /*else
            {
                profile.profile->at(i) = this->threshold_crypto_provider->EncryptInteger(d);
            }*/
        }

        else
        {
            profile.profile.at(i) = this->threshold_crypto_provider->GetEncryptedZero(true);
        }
    }

    return profile;
}

BigInteger UserImpersonator::decrypt_ad(BidResponse const& response)
{
    return this->crypto_provider->DecryptInteger(response.ad);
}

DataPacker::PackedData UserImpersonator::get_model_update(CompressedProfile const& profile, const ahead::BidResponse& response, unsigned int click)
{
    Paillier::Ciphertext delta;
    if (click)
    {
        delta = response.yhat + this->encrypted_minus_one;
    }
    else
    {
        delta = response.yhat;
    }

    //std::cout << "Delta: " << fp::to_double(this->threshold_crypto_provider->DecryptInteger(delta)) << std::endl;
    //std::cout << this->threshold_crypto_provider->DecryptInteger(delta).ToString(16) << std::endl;

    for (unsigned i = 0; i < UserProfile::profile_d; i++)
    {
        if (profile.count(i) && profile.at(i))
        {
            //std::cout << "Packer: " << fp::to_double(this->threshold_crypto_provider->DecryptInteger(delta + delta)) << std::endl;
            //this->packer.push_back(delta * profile.at(i));
            this->packer.push_back(delta);
        }
        else
        {
            this->packer.push_back(0);
        }
    }

    return this->packer.extract();
}


}
