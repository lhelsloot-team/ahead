#ifndef AHEAD_FIXED_POINT_HPP
#define AHEAD_FIXED_POINT_HPP

#include <cmath>
#include <exception>
#include <core/big_integer.h>

class fp
{
public:
    static const size_t w;
    static const size_t p;
    static const size_t mult_factor;
    static const long fp_space;
    static const long signed_boundary;
    static const double min_value;

    static unsigned to_fp(double n);
    static double to_double(unsigned long n);
    static double to_double(SeComLib::Core::BigInteger const& n);
    static SeComLib::Core::BigInteger to_signed_biginteger(double n);
};

#endif
