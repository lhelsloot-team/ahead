
#include <utils/config.h>
#include "timing.hpp"
#include "threshold_paillier.hpp"
#include "dataparser_avazu.hpp"
#include "psp.hpp"
#include "ad_exchange.hpp"
#include "logging.hpp"

using namespace std::chrono;
using namespace SeComLib::Utils;

namespace ahead
{

std::array<std::string, static_cast<unsigned>(timer_id::number_of_timers)> BaseTimer::names{};
std::array<nanoseconds, static_cast<unsigned>(timer_id::number_of_timers)> BaseTimer::elapsed{};
std::array<unsigned, static_cast<unsigned>(timer_id::number_of_timers)> BaseTimer::count{};

Timer<timer_id::update_profile, true> upd_timer("update_profile");
Timer<timer_id::request_ad, true> req_timer("request_ad");
Timer<timer_id::model_update, true> model_timer("update_model");
Timer<timer_id::get_model_update, true> get_model_timer("get_model_update");

BaseTimer::BaseTimer(timer_id id, std::string&& name)
{
    names[static_cast<unsigned>(id)] = std::move(name);
}

void BaseTimer::update(timer_id id, nanoseconds duration, unsigned reps)
{
    unsigned index = static_cast<unsigned>(id);
    elapsed[index] += duration;
    count[index] += reps;
    Logger::GetInstance().push(names[index], duration.count());
}

void BaseTimer::print_timers()
{
    for (unsigned i = 0; i < elapsed.size(); i++)
    {
        std::cout << names[i] << " averaged " << static_cast<double>(elapsed[i].count()) / (1e6 * count[i]) << " ms over " << count[i] << " runs." << std::endl;
    }
}

void timing_main()
{
    std::string training_data = Config::GetInstance().GetParameter<std::string>("HomomorphicAdsTiming.training_data");
    unsigned training_samples = Config::GetInstance().GetParameter<unsigned>("HomomorphicAdsTiming.training_samples");
    unsigned dsp_count = Config::GetInstance().GetParameter<unsigned>("HomomorphicAdsTiming.dsp_count");
    unsigned campaigns_per_dsp = Config::GetInstance().GetParameter<unsigned>("HomomorphicAdsTiming.campaigns_per_dsp");


    std::cout << "Generating keys" << std::endl;

    std::shared_ptr<PSP> psp = std::make_shared<PSP>();
    ThresholdPaillier::PrivateKeyShare key_share = psp->generate_key_shares();

    std::shared_ptr<ThresholdPaillier> dsp_crypto = std::make_shared<ThresholdPaillier>(psp->get_public_key(), key_share);
    std::shared_ptr<ThresholdPaillier> user_crypto = std::make_shared<ThresholdPaillier>(psp->get_public_key());

    std::cout << "Setting up " << dsp_count << " DSPs with " << campaigns_per_dsp << " campaigns each" << std::endl;

    std::shared_ptr<AdExchange> ad_exchange = std::make_shared<AdExchange>(psp, dsp_crypto);
    psp->set_other_party(ad_exchange);

    std::vector<std::shared_ptr<DSP>> all_dsps;
    all_dsps.reserve(dsp_count);

    for (unsigned i = 0; i < dsp_count; i++)
    {
        std::shared_ptr<DSP> dsp = ad_exchange->create_dsp();
        for (unsigned j = 0; j < campaigns_per_dsp; j++)
        {
            dsp->add_campaign(j + i*campaigns_per_dsp, j + i*campaigns_per_dsp, 1, 0);
        }

        dsp->set_other_party(psp);
        all_dsps.push_back(dsp);
    }

    UserImpersonator users(user_crypto);

    std::cout << "Opening data reader" << std::endl;
    DataReader reader(training_data, std::unique_ptr<AvazuDataParser>(new AvazuDataParser()));


    unsigned lines = 0;
    DataLine data;
    while (lines < training_samples && reader.get_line(data))
    {
        lines++;

        upd_timer.start();
        users.update_profile(psp, all_dsps, data);
        upd_timer.stop();

        req_timer.start();
        BidResponse result = ad_exchange->request_ad(data.user_id);
        req_timer.stop();

        model_timer.start();
        get_model_timer.start();
        DataPacker::PackedData model_update = users.get_model_update(users.get_profile(data.user_id), result, data.label);
        get_model_timer.stop();
        psp->aggregate_update(std::move(model_update), result.campaign_id, result.bid_price);
        model_timer.stop();

        Logger::GetInstance().flush();

        if (lines % 1 == 0)
        {
            BaseTimer::print_timers();
        }
    }
}

}
