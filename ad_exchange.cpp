#include "ad_exchange.hpp"

namespace ahead
{

AdExchange::AdExchange(std::shared_ptr<PSP> psp_arg, std::shared_ptr<ThresholdPaillier> crypto)
    : DSPBase(psp_arg, crypto)
    , dgk_crypto_provider(psp_arg->get_dgk_public_key())
    , comparison_server(std::make_shared<SecureComparisonServer>(*(this->crypto_provider), this->dgk_crypto_provider, "HomomorphicAds"))
{
    this->comparison_server->SetClient(this->psp->comparison_client);
    this->psp->comparison_client->SetServer(this->comparison_server);
}

std::shared_ptr<DSP> AdExchange::create_dsp()
{
    this->dsps.push_back(std::make_shared<DSP>(this->psp, this->crypto_provider, this->paillier_crypto_provider));
    return this->dsps.back();
}

BidResponse AdExchange::request_ad(unsigned user_id)
{
    this->psp->start_bidding();

    for (auto it = this->dsps.begin(); it != this->dsps.end(); it++)
    {
        (*it)->bid(user_id);
    }

    auto& bids = this->psp->collect_bids();
    return this->perform_auction(bids);
}

BidResponse AdExchange::request_ad(UserProfile const& user)
{
    this->psp->start_bidding();

    for (auto it = this->dsps.begin(); it != this->dsps.end(); it++)
    {
        (*it)->bid(user);
    }

    auto& bids = this->psp->collect_bids();
    return this->perform_auction(bids);
}

BidResponse AdExchange::perform_auction(std::vector<BidResponse>& bids)
{
    size_t s = bids.size();

    if (s == 0)
    {
        std::cout << "Received no bids!" << std::endl;

        BidResponse response;
        response.has_ad = false;
        response.bid_price = this->crypto_provider->GetEncryptedZero(true);
        response.yhat = this->crypto_provider->GetEncryptedZero(true);
        response.campaign_id = this->crypto_provider->GetEncryptedZero(true);
        return response;
    }

    else if (s == 1)
    {
        return bids[0];
    }

    else
    {
        unsigned i_max = 0;
        for (unsigned i = 1; i < s; i++)
        {
            Paillier::Ciphertext result_enc = this->comparison_server->Compare(bids[i_max].bid_price, bids[i].bid_price);
            unsigned result = this->crypto_provider->DecryptInteger(result_enc).ToUnsignedLong();
            if (result == 0)
            {
                i_max = i;
            }
        }
        return bids[i_max];
    }
}

}
