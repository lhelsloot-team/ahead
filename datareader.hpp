#ifndef AHEAD_DATAREADER_HPP
#define AHEAD_DATAREADER_HPP

#include <string>
#include <memory>
#include <vector>
#include <fstream>

namespace ahead
{
struct DataLine
{
    typedef std::pair<std::string, std::string> Element;
    typedef std::vector<Element> Profile;

    unsigned label;
    uint64_t user_id;
    Profile profile_data;
};

class DataParser
{
public:
    virtual ~DataParser() {}
    virtual void initialize ( std::istream& is ) { (void) is; }
    virtual bool parse_line ( std::istream& is, DataLine& data_line ) = 0;
};

class DataReader
{
public:
    DataReader ( std::string const& file_path, std::unique_ptr<DataParser> data_parser )
        : file_stream(file_path), parser(std::move(data_parser))
    {
        this->parser->initialize(file_stream);
    }

    bool get_line(DataLine& data_line)
    {
        return this->parser->parse_line(file_stream, data_line);
    }

protected:
    std::ifstream file_stream;
    std::unique_ptr<DataParser> parser;
};

}

#endif

