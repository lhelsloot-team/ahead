#include "dataparser_avazu.hpp"
#include <limits>
#include <sstream>
#include "MurmurHash3.h"

namespace ahead
{

void AvazuDataParser::initialize(std::istream& is)
{
    // Ignore the first line of the input file stream
    //is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    // Parse the first line to obtain column labels
    std::string header, element;
    std::getline(is, header);
    std::stringstream line(header);

    while (std::getline(line, element, ','))
    {
        this->columns.push_back(element);
    }
}

bool AvazuDataParser::parse_line(std::istream& is, DataLine& data)
{
    bool success = false;
    data.profile_data.clear();

    // Read a single line
    std::string line_str, element;
    if (std::getline(is, line_str))
    {
        std::stringstream line(line_str);

        // First element is the ad id, appears to be unique so ignore
        line.ignore(std::numeric_limits<std::streamsize>::max(), ',');

        // Second element is the label
        char sep;
        line >> data.label;
        line >> sep;

        // Iterate over line to get rest of the elements
        for (auto it = this->columns.begin() + 2; it != this->columns.end(); it++)
        {
            std::getline(line, element, ',');
            data.profile_data.push_back(std::make_pair(*it, element));
        }

        std::string user_id;
        auto device_id = data.profile_data[9].second;
        if (device_id == "a99f214a")
        {
            user_id = data.profile_data[10].second + data.profile_data[11].second;
            //MurmurHash3_x86_32(user_id.c_str(), user_id.length(), 42, &data.user_id);
        }
        else
        {
            user_id = device_id;
        }

        size_t pos;
        data.user_id = std::stoull(user_id, &pos, 16);

        data.profile_data.push_back(std::make_pair("user_id", user_id));

        success = true;
    }
    return success;
}


}
