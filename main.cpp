#include <iostream>
#include <cmath>
#include <chrono>
#include <utils/cpu_timer.h>
#include <utils/config.h>
#include "threshold_paillier.hpp"

#include "dataparser_avazu.hpp"
#include "user.hpp"
#include "psp.hpp"
#include "ad_exchange.hpp"
#include "data_packer.hpp"
#include "timing.hpp"

using namespace SeComLib::Core;
using namespace SeComLib::Utils;
using namespace ahead;

#ifdef MEASURE_RUNTIME
int main()
{
    timing_main();

    return 0;
}

#else

int main()
{
    std::cout << "Generating keys" << std::endl;

    std::shared_ptr<PSP> psp = std::make_shared<PSP>();
    ThresholdPaillier::PrivateKeyShare key_share = psp->generate_key_shares();

    std::shared_ptr<ThresholdPaillier> dsp_crypto = std::make_shared<ThresholdPaillier>(psp->get_public_key(), key_share);
    std::shared_ptr<ThresholdPaillier> user_crypto = std::make_shared<ThresholdPaillier>(psp->get_public_key());

    AdExchange ad_exchange(psp, dsp_crypto);

    std::shared_ptr<DSP> dsp = ad_exchange.create_dsp();
    std::vector<std::shared_ptr<DSP>> all_dsps(1, dsp);
    dsp->add_campaign(1, 1, 1, 1);
    dsp->add_campaign(2, 2, 1, 2);

    psp->set_other_party(dsp);
    dsp->set_other_party(psp);

    std::cout << "Opening data reader" << std::endl;

    DataLine data;
    DataReader reader(Config::GetInstance().GetParameter("HomomorphicAds.training_data", std::string("train.csv")), std::unique_ptr<AvazuDataParser>(new AvazuDataParser()));
    UserImpersonator users(user_crypto);

    std::cout << "Reading file" << std::endl;
    double logloss[] = { 0., 0. };
    unsigned count[] = { 0, 0 };
    unsigned lines = 0;

    std::cout << "Profile dimensionality: " << UserProfile::profile_d << std::endl;

/*
    std::cout << std::hex << fp::to_fp(2) << std::endl;
    std::cout << fp::to_double(fp::to_fp(0.5)) << std::endl;
    std::cout << fp::to_fp(-1) << std::endl;
    std::cout << fp::to_double(fp::to_fp(-2)) << std::endl;

    DataPacker packer(psp->get_crypto_provider(), 16);
    for (unsigned i = 0; i < 32; i++)
    {
        if (i == 3)
        {
            packer.push_back(dsp_crypto->EncryptInteger(fp::to_fp(-static_cast<double>(i) / 10)));
        }
        else
        {
            packer.push_back(fp::to_fp(-static_cast<double>(i) / 10));
        }
    }
    DataPacker::PackedData packed_a = packer.extract();

    for (unsigned i = 0; i < 32; i++)
    {
        packer.push_back(fp::to_fp(-1));
    }
    DataPacker::PackedData packed_b = packer.extract();

    packed_a += packed_b;
    DataPacker::UnpackedData unpacked = packer.unpack(packed_a);
    unsigned i = 0;
    for (auto it = unpacked.begin(); it != unpacked.end(); it++)
    {
        std::cout << fp::to_double(*it) << std::endl;
    }
*/

    CpuTimer::NanosecondType profile_creation_time = 0;
    CpuTimer::NanosecondType request_ad_time = 0;
    CpuTimer::NanosecondType model_update_time = 0;

    CpuTimer timer;

    while (reader.get_line(data))
    {
        lines++;

        timer.Reset();
        //dsp->add_user(data.user_id, users.crypto_provider);
        //CompressedProfile cp = users.get_hashed_profile(data);
        //EncryptedCompressedProfile ep = users.encrypt_hashed_profile(cp);
        //psp->expand_profile(dsp, ep, user_crypto->EncryptInteger(data.user_id));
        //UserProfile up = users.get_encrypted_profile(cp);
        users.update_profile(psp, all_dsps, data);
        timer.Stop();
        profile_creation_time += timer.GetDuration();


        timer.Reset();
        BidResponse result = ad_exchange.request_ad(data.user_id);
        timer.Stop();
        request_ad_time += timer.GetDuration();

        if (result.has_ad)
        {
            unsigned winner = users.decrypt_ad(result).ToUnsignedLong() - 1;
            count[winner]++;

            double yhat = fp::to_double(dsp_crypto->DecryptInteger(result.yhat));
            yhat = std::max(std::min(yhat, 1. - 10e-12), 10e-12);

            if (data.label)
            {
                logloss[winner] -= std::log(yhat);
            }
            else
            {
                logloss[winner] -= std::log(1. - yhat);
            }

            if (lines % 10 == 0)
            {
                for (unsigned i = 0; i <= 1; i++)
                {
                    std::cout << i << " after " << count[i] << " samples - logloss: " << (logloss[i] / count[i]) << std::endl;
                }


                std::cout << "Profile creation time: " << static_cast<double>(profile_creation_time / lines) / 1e9 << "s" << std::endl;
                std::cout << "Ad selection time: " << static_cast<double>(request_ad_time / lines) / 1e9 << " s" << std::endl;
                std::cout << "Model update time: " << static_cast<double>(model_update_time / lines) / 1e9 << " s" << std::endl;

                std::cout << std::endl;
            }

            timer.Reset();
            DataPacker::PackedData model_update = users.get_model_update(users.get_profile(data.user_id), result, data.label);
            psp->aggregate_update(std::move(model_update), result.campaign_id, result.bid_price);
            timer.Stop();
            model_update_time += timer.GetDuration();
        }

        else
        {
            std::cout << "No ad received!" << std::endl;
        }
    }

    std::cout << "Total profile creation time: " << profile_creation_time / 1000 << "ms" << std::endl;
    std::cout << "Total ad selection time: " << request_ad_time / 1000 << " ms" << std::endl;
    std::cout << "Total model update time: " << model_update_time / 1000 << " ms" << std::endl;

    return 0;
}

#endif

#if false

#define TIME_DECRYPT 1
#define TIME_SHARE_DECRYPT 0

int main(int argc, char **argv) {
	const int n = 10000;
	CpuTimer::NanosecondType duration;
	CpuTimer::NanosecondType per_cycle;
	BigInteger x(15);
	CpuTimer timer;

	ThresholdPaillier paillier;

	std::cout << "Generating keys.. " << std::endl;
	timer.Reset();
	ThresholdPaillier::PrivateKeyShare key_share = paillier.GenerateKeyShares();
	timer.Stop();
	std::cout << timer.GetDuration() << " ns" << std::endl;

	ThresholdPaillier opaillier(paillier.GetPublicKey(), key_share);
	ThresholdPaillier ppaillier(paillier.GetPublicKey());

	for (int i = 0; i < 1000; i++)
	{
		Paillier::Ciphertext encX = ppaillier.EncryptInteger(x);
		Paillier::Ciphertext encY = opaillier.EncryptInteger(i);
		Paillier::Ciphertext encZ = encX + encY;

		Paillier::Ciphertext zA = paillier.ShareDecrypt(encZ);
		Paillier::Ciphertext zB = opaillier.ShareDecrypt(encZ);
		BigInteger zr = paillier.CombineShares(zA, zB);
		if (zr != x+i)
		{
			std::cout << "Decryption failed at i = " << i << ", result = " << zr.ToString(10) << std::endl;
			exit(1);
		}
	}


	Paillier::Ciphertext encX = paillier.EncryptInteger(x);
	Paillier::Ciphertext encY = paillier.EncryptInteger(5);
	Paillier::Ciphertext encZ = encX + encY;

	std::cout << "Message space size in bits: " << paillier.GetMessageSpaceSize() << std::endl;

	std::cout << "Performing " << n << " encryptions... " << std::flush;
	timer.Reset();

	for (int i = 0; i < n; i++) {
		paillier.EncryptInteger(i);
	}

	timer.Stop();
	duration = timer.GetDuration();
	per_cycle = duration / n;
	std::cout << duration << " ns → " << per_cycle << " ns per cycle" << std::endl;

#if TIME_DECRYPT
	std::cout << "Performing " << n << " decryptions... " << std::flush;
	timer.Reset();

	for (int i = 0; i < n; i++) {
		paillier.DecryptInteger(encX);
	}

	timer.Stop();
	duration = timer.GetDuration();
	per_cycle = duration / n;
	std::cout << duration << " ns → " << per_cycle << " ns per cycle" << std::endl;
#endif

#if TIME_SHARE_DECRYPT
	std::cout << "Performing " << n << " share decryptions... " << std::flush;
	timer.Reset();

	for (int i = 0; i < n; i++) {
		paillier.ShareDecrypt(encX, paillier.d0);
	}

	timer.Stop();
	duration = timer.GetDuration();
	per_cycle = duration / n;
	std::cout << duration << " ns → " << per_cycle << " ns per cycle" << std::endl;


	Paillier::Ciphertext c0 = paillier.ShareDecrypt(encX, paillier.d0);
	Paillier::Ciphertext c1 = paillier.ShareDecrypt(encX, paillier.d1);

	std::cout << "Performing " << n << " share combinations... " << std::flush;
	timer.Reset();

	for (int i = 0; i < n; i++) {
		paillier.CombineShares(c0, c1);
	}

	timer.Stop();
	duration = timer.GetDuration();
	per_cycle = duration / n;
	std::cout << duration << " ns → " << per_cycle << " ns per cycle" << std::endl;
#endif

	std::cout << "Performing " << n << " randomizations... " << std::flush;
	timer.Reset();

	for (int i = 0; i < n; i++) {
		paillier.RandomizeCiphertext(encX);
	}

	timer.Stop();
	duration = timer.GetDuration();
	per_cycle = duration / n;
	std::cout << duration << " ns → " << per_cycle << " ns per cycle" << std::endl;


	std::cout << "Performing " << n << " multiplications... " << std::flush;
	timer.Reset();

	for (int i = 0; i < n; i++) {
		encZ = encY + encX;
	}

	timer.Stop();
	duration = timer.GetDuration();
	per_cycle = duration / n;
	std::cout << duration << " ns → " << per_cycle << " ns per cycle" << std::endl;


	int const c = 4096;
	std::cout << "Performing " << n << " exponentiations... " << std::flush;
	timer.Reset();

	for (int i = 0; i < n; i++) {
		encZ = encX * c;
	}

	timer.Stop();
	duration = timer.GetDuration();
	per_cycle = duration / n;
	std::cout << duration << " ns → " << per_cycle << " ns per cycle" << std::endl;

    return 0;
}

#endif
