#ifndef AHEAD_AHEAD_HPP
#define AHEAD_AHEAD_HPP

#include <vector>
#include "fixed_point.hpp"
#include "threshold_paillier.hpp"

namespace ahead
{
	typedef std::vector<ThresholdPaillier::Ciphertext> EncryptedVector;
	typedef std::vector<ThresholdPaillier::PartialDecryption> PartiallyDecryptedVector;
	typedef std::vector<ThresholdPaillier::Ciphertext> EncryptedArray;
}

#endif
