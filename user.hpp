#ifndef AHEAD_USER_HPP
#define AHEAD_USER_HPP

#include <memory>
#include <unordered_map>

#include "ahead.hpp"
#include "datareader.hpp"
#include "threshold_paillier.hpp"
#include "bid.hpp"
#include "data_packer.hpp"

namespace ahead
{

class DSP;
class PSP;

typedef std::unordered_map<unsigned, int> CompressedProfile;

struct EncryptedCompressedProfile
{
    std::unordered_map<unsigned, ThresholdPaillier::Ciphertext> data;
    Paillier::Ciphertext r;
};

class UserProfile
{
public:
    UserProfile() {}
    UserProfile(std::shared_ptr<Paillier> crypto);

	std::shared_ptr<Paillier> crypto_provider;
	EncryptedArray profile;

    const static size_t profile_bits;
    const static size_t profile_d;
};

class User
{
public:
    User() : count(0) {}

    unsigned get_count() const { return count; }

    void update_profile(CompressedProfile const& update);
    CompressedProfile get_profile_diff();
    CompressedProfile const& get_profile() const { return this->profile; }

//private:
    unsigned count;
    CompressedProfile profile;
#ifdef USE_INCREMENTAL_UPDATES
    CompressedProfile diff;
#endif
};

class UserImpersonator
{
public:

    UserImpersonator(std::shared_ptr<ThresholdPaillier> threshold_crypto);

    void update_profile(std::shared_ptr<PSP> psp, std::vector<std::shared_ptr<DSP>> const& dsps, DataLine const& data_line);
    CompressedProfile const& get_profile(unsigned user_id) const;

    CompressedProfile get_hashed_profile(DataLine const& data_line);
    EncryptedCompressedProfile encrypt_hashed_profile(CompressedProfile const& profile, std::shared_ptr<Paillier> dsp_crypto);

    UserProfile get_encrypted_profile(CompressedProfile const& compressed_profile);

    BigInteger decrypt_ad(BidResponse const& response);

    DataPacker::PackedData get_model_update(CompressedProfile const& profile, BidResponse const& response, unsigned click);

//protected:
    uint32_t const seed;

    Paillier dsp_crypto_provider;
    std::shared_ptr<Paillier> crypto_provider;
    std::shared_ptr<ThresholdPaillier> threshold_crypto_provider;
    std::unordered_map<unsigned, User> users;

    Paillier::Ciphertext encrypted_minus_one;
    DataPacker packer;
    static const unsigned profile_update_interval;
};

}

#endif

