#include "dsp.hpp"
#include "psp.hpp"
#include "timing.hpp"
#include <cmath>
#include <core/secure_permutation.h>
#include <utils/config.h>

namespace ahead
{
    using namespace SeComLib::Utils;

    const unsigned PSP::model_update_interval = Config::GetInstance().GetParameter<unsigned>("HomomorphicAds.model_update_interval", 10);

    Timer<timer_id::expand_profile, true> expand_timer("expand_profile");

    ThresholdPaillier::PrivateKeyShare PSP::generate_key_shares()
    {
        this->dgk_crypto_provider.GenerateKeys();
        ThresholdPaillier::PrivateKeyShare key_share = this->crypto_provider->GenerateKeyShares();
        this->comparison_client = std::make_shared<SecureComparisonClient>(*this->crypto_provider, this->dgk_crypto_provider, "HomomorphicAds");
        return key_share;
    }

    DgkPublicKey const& PSP::get_dgk_public_key() const
    {
        return this->dgk_crypto_provider.GetPublicKey();
    }

    void PSP::expand_profile(std::shared_ptr<DSP> dsp, EncryptedCompressedProfile const& profile, Paillier::Ciphertext const& user_id)
    {
        EncryptedArray v = this->do_expand_profile(profile);
        dsp->update_user_profile(std::move(v), user_id, profile.r);
    }

    void PSP::expand_profile(std::vector<std::shared_ptr<DSP>> const& dsps, EncryptedCompressedProfile const& profile, Paillier::Ciphertext const& user_id)
    {
        if (dsps.size() == 1)
        {
            this->expand_profile(dsps[0], profile, user_id);
        }

        else
        {
            EncryptedArray v = this->do_expand_profile(profile);

            for (auto it = dsps.begin(); it != dsps.end(); it++)
            {
                (*it)->update_user_profile(v, user_id, profile.r);
            }
        }
    }

    void PSP::no_expand_profile() const
    {
        expand_timer.skip();
    }

    EncryptedArray PSP::do_expand_profile(EncryptedCompressedProfile const& profile)
    {
        expand_timer.start();
        EncryptedArray v;
        v.reserve(UserProfile::profile_d);
        for (unsigned i = 0; i < UserProfile::profile_d; i++)
        {
            if (profile.data.count(i))
            {
                v.push_back(profile.data.at(i));
            }
            else
            {
                v.emplace_back(this->crypto_provider->GetEncryptedZero(true));
            }
        }
        expand_timer.stop();
        return v;
    }

	EncryptedVector PSP::calculate_sigma(PartiallyDecryptedVector const& enc_s)
	{
		EncryptedVector yhat;
        yhat.reserve(enc_s.size());
		for (size_t i = 0; i < enc_s.size(); i++)
		{
			ThresholdPaillier::DecryptedShare s_share = this->crypto_provider->ShareDecrypt(enc_s[i].first);
			BigInteger s = this->crypto_provider->CombineShares(s_share, enc_s[i].second);

			double x = fp::to_double(s);
			double sigma = 1. / (1. + std::exp(-std::max(std::min(x, 20.), -20.)));

			yhat.emplace_back(this->crypto_provider->EncryptInteger(fp::to_fp(sigma)));
		}

		return yhat;
	}

	void PSP::submit_bid(BidResponse const& bid)
	{
		this->bids.push_back(bid);
	}

	void PSP::start_bidding()
	{
		this->bids.clear();
	}

	std::vector<BidResponse>& PSP::collect_bids()
    {
        SecurePermutation permutation(this->bids.size());
        permutation.Permute(this->bids);
        return this->bids;
    }

    void PSP::clear_model_aggregate(unsigned campaign_id)
    {
        ModelUpdate& update = this->model_aggregate[campaign_id];
        update.model.data.clear();
        update.price = this->crypto_provider->GetEncryptedZero(true);
        update.count = 0;
    }

    void PSP::aggregate_update(DataPacker::PackedData&& g, Paillier::Ciphertext const& campaign_id, ThresholdPaillier::Ciphertext const& bid_price)
    {
        unsigned k = this->paillier_crypto_provider->DecryptInteger(campaign_id).ToUnsignedLong();

        ModelUpdate& update = this->model_aggregate[k];

        if (update.model.data.empty())
        {
            update.model.data.swap(g.data);
        }
        else
        {
            update.model += g;
        }

        update.price = update.price + bid_price;
        update.count += 1;

        if (update.count >= model_update_interval)
        {
            DSP* dsp = DSP::find_dsp(k);
            dsp->update_model(update.model, k);
            this->clear_model_aggregate(k);
        }
    }

}
