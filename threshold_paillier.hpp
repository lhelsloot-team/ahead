#ifndef AHEAD_THRESHOLD_PAILLIER_HPP
#define AHEAD_THRESHOLD_PAILLIER_HPP

#include <utility>
#include "secomlib.hpp"

namespace ahead
{
	using namespace SeComLib::Core;

    class ThresholdPaillierParty;

	/**
	 * @brief Implementation of a two-party Paillier cryptosystem.
	 */
	class ThresholdPaillier : public Paillier
	{
	public:
		typedef BigInteger PrivateKeyShare;
		typedef Ciphertext DecryptedShare;
		typedef std::pair<Ciphertext, DecryptedShare> PartialDecryption;

		/// Default constructor.
		ThresholdPaillier();

		/// Creates a ThresholdPaillier provider for encryption and homomorphic operations.
		ThresholdPaillier(PaillierPublicKey const& public_key);

		/// Creates a ThresholdPaillier provider for encryption, homomorphic operations and share decryptions.
		ThresholdPaillier(PaillierPublicKey const& public_key, PrivateKeyShare const& key_share);

		/// Generate a key pair and split the private key into two shares.
		bool GenerateKeys() override;

		/// Generate key shares, and return one of the shares.
		PrivateKeyShare GenerateKeyShares();

		/// Partially decrypt a ciphertext
		DecryptedShare ShareDecrypt(Ciphertext const& ciphertext) const;

		/// Combine two partial decryptions to obtain the plaintext message.
		BigInteger CombineShares(DecryptedShare const& share1, DecryptedShare const& share2) const;
        BigInteger CombineToBlob(DecryptedShare const& share1, DecryptedShare const& share2) const;

        BigInteger DecryptInteger(Ciphertext const& ciphertext) const override;
        BigInteger DecryptBlob(Ciphertext const& ciphertext) const;

        void SetOtherParty(std::shared_ptr<ThresholdPaillierParty> party);

	private:
		bool has_key_pair, has_key_share;
		PrivateKeyShare key_share;
		BigInteger m, nSquaredMinusOne;

        std::shared_ptr<ThresholdPaillierParty> other_party;
	};


    class ThresholdPaillierParty
    {
    public:
        ThresholdPaillierParty();
        ThresholdPaillierParty(std::shared_ptr<ThresholdPaillier> crypto);
        ThresholdPaillierParty(std::shared_ptr<ThresholdPaillier> crypto, std::shared_ptr<Paillier> paillier_crypto);

        void set_other_party(std::shared_ptr<ThresholdPaillierParty> party);

        PaillierPublicKey const& get_public_key() const;
        PaillierPublicKey const& get_paillier_public_key() const;

        std::shared_ptr<Paillier> get_paillier_crypto() const;

        ThresholdPaillier::DecryptedShare get_share(Paillier::Ciphertext const& ciphertext) const;

    protected:
        void set_crypto_provider(std::shared_ptr<ThresholdPaillier> crypto);

        std::shared_ptr<ThresholdPaillier> crypto_provider;
        std::shared_ptr<Paillier> paillier_public_crypto_provider;
        std::shared_ptr<Paillier> paillier_crypto_provider;

    };
}

#endif

