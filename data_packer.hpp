#ifndef AHEAD_DATA_PACKER_HPP
#define AHEAD_DATA_PACKER_HPP

#include <memory>
#include <vector>
#include "threshold_paillier.hpp"

namespace ahead
{

class DataPacker
{
public:

    class PackedData
    {
    public:
        std::vector<ThresholdPaillier::Ciphertext> data;

        PackedData& operator+= (PackedData const& other);
    };

    typedef std::vector<BigInteger> UnpackedData;

    DataPacker(std::shared_ptr<ThresholdPaillier> crypto, size_t size);

    void push_back(ThresholdPaillier::Ciphertext const& element);
    void push_back(BigInteger const& element);

    PackedData extract();

    UnpackedData unpack(PackedData const& packed_data);

protected:
    void check_size();
    void encrypt_bucket();
    void unpack_bucket(ThresholdPaillier::Ciphertext const& bucket, UnpackedData& unpacked);

    size_t get_element_size() const { return this->data_size + 1; }

    std::shared_ptr<ThresholdPaillier> crypto_provider;
    size_t data_size;
    size_t elements_per_bucket;
    bool buffer_contains_encryption = false;

    size_t buffer_size = 0;
    BigInteger plaintext_buffer = 0;
    ThresholdPaillier::Ciphertext ciphertext_buffer;
    PackedData packed_data;
};

}

#endif

